﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/data")]
    public class IdentityController : ControllerBase
    {
        [Authorize]
        [HttpGet("secret")]
        public IActionResult Get()
        {
            return Ok("Secret");
        }
        [HttpGet("public")]
        public IActionResult GetPublic()
        {
            return Ok("Public");
        }
    }
}
