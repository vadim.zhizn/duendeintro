﻿using IdentityModel.Client;
using Microsoft.AspNetCore.Mvc;

namespace Client.Controllers
{
    public class HomeController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public HomeController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<IActionResult> Index()
        {
            var authClient = _httpClientFactory.CreateClient();
            var dicoveryDocument = await authClient.GetDiscoveryDocumentAsync("https://localhost:5001");

            var tokenResponse = await authClient.RequestClientCredentialsTokenAsync(
                new ClientCredentialsTokenRequest
                {
                    Address = dicoveryDocument.TokenEndpoint,
                    ClientId = "client_id",
                    ClientSecret = "client_secret",
                    Scope = "WebApi"
                });

            var webApiClient = _httpClientFactory.CreateClient();
            webApiClient.SetBearerToken(tokenResponse.AccessToken);

            var response = await webApiClient.GetAsync("https://localhost:6001/api/data/secret");
            var message = await response.Content.ReadAsStringAsync();
            return Ok(message);
        }
    }
}
